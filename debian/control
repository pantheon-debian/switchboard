Source: switchboard
Section: admin
Priority: optional
Maintainer: elementary OS Team <builds@elementary.io>
Build-Depends: cmake (>= 2.8),
               debhelper (>= 10.5.1),
               intltool,
               libclutter-gtk-1.0-dev,
               libgee-0.8-dev (>= 0.5.3),
               libglib2.0-dev (>= 2.26.0),
               libgranite-dev (>= 0.3.0~),
               libgtk-3-dev (>= 3.10.0),
               valac (>= 0.22)
Standards-Version: 4.1.1
Homepage: https://github.com/elementary/switchboard

Package: switchboard
Architecture: any
Depends: libswitchboard-2.0-0 (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Pre-Depends: dpkg (>= 1.15.6)
Description: Modular Settings Hub
 Switchboard is a modular system settings hub designed
 for Pantheon desktop.
 .
 It features animated transitions, supports adding or
 removing plugs (settings panels) without recompilation,
 and follows the Human Interface Guidelines of
 elementary OS.

Package: libswitchboard-2.0-dev
Section: libdevel
Architecture: any
Depends: libswitchboard-2.0-0 (= ${binary:Version}), ${misc:Depends}
Description: Modular Settings Hub - development files
 Shared library to create plugs (settings panels) for Switchboard.
 .
 Switchboard is a modular system settings hub designed
 for Pantheon desktop.
 .
 It features animated transitions, supports adding or
 removing plugs (settings panels) without recompilation,
 and follows the Human Interface Guidelines of
 elementary OS.
 .
 This package contains the header and development files which are
 needed for building Switchboard plugs.

Package: libswitchboard-2.0-0
Section: libs
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: Modular Settings Hub - shared libraries
 Shared library to create plugs (settings panels) for Switchboard.
 .
 Switchboard is a modular system settings hub designed
 for Pantheon desktop.
 .
 It features animated transitions, supports adding or
 removing plugs (settings panels) without recompilation,
 and follows the Human Interface Guidelines of
 elementary OS.
 .
 This package contains the shared library required to run the plugs.
